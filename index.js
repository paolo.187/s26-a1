const http = require('http');

const PORT = 3000;

http.createServer(function(request,response){

		if(request.url == '/login'){

			response.writeHead(200,{'Content-Type': 'text/plain'})
			response.end("Welcome to Login Page")
		} else {

			response.writeHead(404,{'Content-Type': 'text/plain'})
			response.end("Sorry, the page you are looking for cannot be found.")
		}


}).listen(PORT)

console.log(`Server is running at localhost: ${PORT}.`)


/*
Questions:

a. What directive is used by NodeJS in loading modules it needs?
	"require"
			
b. What Node.js module contains a method for server creation?
	- .createServer

c. What is the method of the http object reponsible for creating a server using Node.js?
	- .createServer
d. What method of the response object allows us to set status codes and content types?
	- .writeHead
e. Where will console.log() output its contents when run in node.js?

		- in the terminal (gitbash)

f. What property of the request object contains the address endpoint?
		- .end





*/